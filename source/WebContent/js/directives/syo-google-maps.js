/**
 * Modulo syoGoogleMaps.
 * Define as diretivas que representam o mapa e os marcadores.
 * Um mapa pode ter n marcadores.
 *    
 */
angular.module("syoGoogleMaps", [])
.directive("syoGoogleMaps", function (syoGoogleMapsAPI) {
	return {
		restrict : "AE",
		transclude : true,
		scope : {},
		controller : function ($scope, $window, $q, lazyLoadMapApi) {
			
			console.log('syoGoogleMaps', this);			
			
			this.initMap = function(coordenadas) {
				console.log('Checkpoint: syoGoogleMaps controller initMap', coordenadas);
				
				var deferred = $q.defer();
				
				lazyLoadMapApi
					.then(function(result) {
						console.log('Successo: lazyLoadMapApi ' + result);
						
						var map = syoGoogleMapsAPI.posicionaMapa(coordenadas);					
						deferred.resolve(map);
					});
				
				return deferred.promise;				
			};
			
		},
		templateUrl : 'templates/map-template.html'
	}
})
.directive("syoGoogleMapsMarcador", function (syoGoogleMapsAPI) {
	return {
		require : '^^syoGoogleMaps',
		restrict : "AE",
		transclude : true,
		scope : {},
		link : function (scope, element, attrs, syoGoogleMapsController) {
			console.log('Checkpoint: syoGoogleMapsMarcador link attrs', attrs);
			
			aCoords = scope.$eval(attrs.coordenadas);
			scope.coord = {
					latitude: aCoords[0], 
					longitude: aCoords[1]
			}
			scope.textinfo = attrs.textinfo;
			
			console.log('syoGoogleMapsMarcador', this);
			
			syoGoogleMapsController.initMap(scope.coord)
				.then(function(map) {
					console.log('Mapa posicionado, adicionar marcador: ', scope.textinfo);
					syoGoogleMapsAPI.addMarcador(scope.coord, scope.textinfo);
				});
		}
	}
})

