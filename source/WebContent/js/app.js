/**
 * Script principal da aplica��o.
 * 
 * As coordenadas devem ser informadas no atributo da diretiva da seguinte forma:
 * coordenadas="[valor-da-latitude, valor-da-longitude]"
 * Ex.: coordenadas="[-29.6838927, -51.4576999]"
 * 
 * Podem ser informadas quantas coordenadas desejar.
 * 
 * Na cria��o da app � injetada a diretiva syoGoogleMaps, que injeta o servi�o syoGoogleMapsAPI.   
 * A API do Google Maps � carregada dinamicamente pela syoGoogleMapsAPI, via servi�o lazyLoadMapApi.
 *    
 */
angular.module("app", ["syoGoogleMaps"])
	.controller("Controller", function () {
		console.log('App inicializada');
	});