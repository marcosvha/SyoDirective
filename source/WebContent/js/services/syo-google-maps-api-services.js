angular.module("syoGoogleMaps")
.factory("syoGoogleMapsAPI", function($window, syoGoogleMapsConfig, lazyLoadMapApi) {
	var _map = 'undefined';

	var _posicionaMapa = function(coordenadas) {
		if (_map === 'undefined') {
			console.log('Checkpoint: _posicionaMapa Coordenadas do mapa', coordenadas);
			console.log('syoGoogleMapsConfig.mapZoom', syoGoogleMapsConfig.mapZoom);
		
			if (google === "undefined") {
				console.log('API do Google Maps n�o foi carregada corretamente.');
				alert('API do Google Maps n�o foi carregada corretamente.');
				return false;
			}		
			
			var centroDoMapa = {
					lat: coordenadas.latitude,
					lng: coordenadas.longitude
			};
	
			_map = new google.maps.Map(document.getElementById('map'), {
				zoom: syoGoogleMapsConfig.mapZoom,
				center: centroDoMapa
			});
		}

		return _map;
	};

	var _addMarcador = function(coordenadas, info) {
		console.log('Checkpoint: _addMarcador Coordenadas do marcador', coordenadas);
		console.log('info', info);

		if (_map === "undefined") {
			_posicionaMapa(coordenadas);
		}

		var posicaoMarcador = {
				lat: coordenadas.latitude,
				lng: coordenadas.longitude
		};    	

		var marker = new google.maps.Marker({
			position: posicaoMarcador,
			map: _map,
			label: info
		});
	};

	return {
		posicionaMapa: _posicionaMapa,
		addMarcador: _addMarcador
	};
})
.service('lazyLoadMapApi', function lazyLoadMapApi($window, $q, $timeout, syoGoogleMapsConfig) {

	function carregaScriptGoogleMapsAPI() {
		console.log('Checkpoint: lazyLoadMapApi carregaScriptGoogleMapsAPI');

		var sScriptSrc = syoGoogleMapsConfig.googleMapsAPIUrl;
		sScriptSrc += '?key=' + syoGoogleMapsConfig.googleMapsAPIKey;
		sScriptSrc += '&callback=inicializaMapa';
		console.log('sScriptSrc', sScriptSrc);
		
		var elScript = document.createElement('script');
		elScript.src = sScriptSrc;
		elScript.setAttributeNode(document.createAttribute("async"));
		elScript.setAttributeNode(document.createAttribute("defer"));
		
		document.body.appendChild(elScript);
	}
	
	console.log('Checkpoint: lazyLoadMapApi');
	
	var deferred = $q.defer();

	$window.inicializaMapa = function() {
		// Define a fun��o de callback chamada quando o script do Google Maps termina de carregar
		console.log('Checkpoint: inicializaMapa callback');
		
		if (typeof(google) === 'undefined') {
			console.log('API do google.maps ainda n�o inicializada.');
			
			$timeout(function () {
				console.log('Chamando recursivamente inicializaMapa()');
				$window.inicializaMapa();
			}, 1000);
		} else {
			deferred.resolve('API google.maps est� presente!');
		}
	}

	if ($window.attachEvent) {
		$window.attachEvent('onload', carregaScriptGoogleMapsAPI);
	} else {
		$window.addEventListener('load', carregaScriptGoogleMapsAPI, false);
	}

	return deferred.promise;
});