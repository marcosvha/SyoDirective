### Desafio diretiva angular.js

#### SyoGoogleMaps
- [X] Criar um projeto publico em gitlab.com.
- [X] Criar um modulo chamado `SyoDirective`.
- [X] Implementar diretivas para o google maps.

#### O que a diretiva precisa?
- [X] Precisa renderizar o google maps no navegador.
- [X] Precisa receber uma `localização` para exibir ponteiros no mapa.
- [X] Precisa receber um texto para exibir sobre o ponteiro do mapa.

##### Dicas
- [X] Pode ser usado TypeScript ou *JavaScript*.
- [X] Será considerada a funcionalidade.
- [X] Faça commits atômicos.
- [X] Indentação (Sugiro usar algum linter).
- [X] Formatação do código (Sugiro usar algum linter).
- [ ] Build do código.
- [ ] Testes unitários.
- [ ] Crie um docker para executar os testes.

O código está na pasta source, para execução é necessário baixar e executar através um servidor http.

- [ ] ToDo: Estudar Configurar o Auto DevOps do GitLab
